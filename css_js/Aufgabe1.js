// JavaScript source code fuer Aufgabe 1

$(function count() {
    $('#anzahl').click(function count() {
        var zAnzahl = $('#table tr').length-1;        
        $('#count').val(zAnzahl);
		var gAnzahl = $('#table tr td').length;        
        $('#count2').val(gAnzahl);
    });
});


//reset
$(function clearResult() {
	$('#clear').click(function clearResult(){
		$(':text').val('');
	});   
});

//hier kommt das Script zum anfuegen einer weiteren Tabellenzeile
//ohne jQuery

function neueZeile() {
	//referenzier die Tabelle
    let tabelle = document.getElementById('table');

    //fuege eine neue Zeile am ende hinzu
    var nextZeile = tabelle.insertRow(-1);

    //fuege eine zelle in die zeile mit dem Index 0
    let Zelle1 = nextZeile.insertCell(0);
    let Zelle2 = nextZeile.insertCell(1);
    let Zelle3 = nextZeile.insertCell(2);
    let Zelle4 = nextZeile.insertCell(3);

    //textknoten an Zelle anfuegen
    var name = document.getElementById('Name').value;
    var alt = document.getElementById('Alter').value;
    var ges = document.getElementById('Geschlecht').value;
    var haar = document.getElementById('Haarfarbe').value;

    let Text1 = document.createTextNode(name);
    Zelle1.appendChild(Text1);
    let Text2 = document.createTextNode(alt);
    Zelle2.appendChild(Text2);
    let Text3 = document.createTextNode(ges);
    Zelle3.appendChild(Text3);
    let Text4 = document.createTextNode(haar);
    Zelle4.appendChild(Text4);
}

